/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package institute;

import java.util.List;

/**
 *
 * @author DHAIRYA
 */
public class Department {
    private String name;
    private List<Student> students;
    

    public Department(String name) {
        this.name = name;
        this.students = students;
        
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Department(String name, List<Student> students) {
        this.name = name;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public List<Student> getStudents()
    {
        return students;
    }
    
    
}
