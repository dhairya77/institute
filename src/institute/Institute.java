/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package institute;

import java.util.List;

/**
 *
 * @author DHAIRYA
 */
public class Institute {
    private String name;
    private List<Department> departments;
    
    

    public Institute(String name, List<Department>departments) {
        this.name = name;
        this.departments = departments;
        
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
